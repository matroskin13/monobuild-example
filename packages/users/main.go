package main

import (
	"fmt"
	"monorepo-gitlab/packages/books"
)

type user struct {
	id    int
	books []books.Book
}

func main() {
	users := []user{
		{
			id: 1,
			books: []books.Book{
				{Id: 1, Title: "The Adventures of Tom Sawyer"},
				{Id: 2, Title: "The Adventures of Huckleberry Finn"},
			},
		},
	}

	fmt.Println(users)
}
